package br.com.narguishow.nargui.componentes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import br.com.narguishow.nargui.services.LeituraFormulariosService;

@Component
public class AgendadorDeTarefas {
	
	@Autowired
	private LeituraFormulariosService leituraFormulariosService;

	@Scheduled(cron = "${task.formulario.pendente}")
	public void fomulariosPendentes() throws Exception {
		//leituraFormulariosService.getRespostaFormularioPendentes();
	}
}
