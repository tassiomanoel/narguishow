package br.com.narguishow.nargui.rest;

import java.awt.image.BufferedImage;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import br.com.narguishow.nargui.services.LeituraFormulariosService;
import br.com.narguishow.nargui.to.RespostaFormularioDigitadoTO;
import br.com.narguishow.nargui.to.RespostaFormularioTO;

@RestController
@RequestMapping("/respostaformulario")
public class RespostaFormularioRestController {

	@Autowired private LeituraFormulariosService leituraFormulariosService;

	@RequestMapping(value = "/salvarimagem", method = RequestMethod.POST)
	public void salvarImagem(HttpServletRequest httpServletRequest) throws Exception {
		
		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		MultipartHttpServletRequest multipartHttpServletRequest = multipartResolver.resolveMultipart(httpServletRequest);
		Integer idPdf = Integer.valueOf(multipartHttpServletRequest.getParameter("idPdf"));
		BufferedImage bi = null;
		
		for (MultipartFile mf : multipartHttpServletRequest.getFileMap().values()) {
			bi = ImageIO.read(mf.getInputStream());
			leituraFormulariosService.salvar(idPdf, bi);
		}
	}
	
	@RequestMapping(value = "/listarrespostasperiodo", method = RequestMethod.GET)
	public ResponseEntity<List<RespostaFormularioTO>> listarRespostaFormularioPorPeriodo(Date dataInicio, Date dataFim) throws Exception {
		return new ResponseEntity<List<RespostaFormularioTO>>(leituraFormulariosService.listaRespostaFormularioPorPeriodo(dataInicio, dataFim), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getContadorRespostaFormulario", method = RequestMethod.GET)
	public ResponseEntity<List<Integer>> getContadorRespostaFormulario() throws Exception {
		return new ResponseEntity<List<Integer>>(leituraFormulariosService.getContadorRespostaFormulario(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getRespostaFormularioTextoNulo", method = RequestMethod.GET)
	public ResponseEntity<List<RespostaFormularioDigitadoTO>> getRespostaFormularioTextoNulo() throws Exception {
		return new ResponseEntity<List<RespostaFormularioDigitadoTO>>(leituraFormulariosService.getRespostaFormularioTextoNulo(), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RespostaFormularioDigitadoTO> salvar(@RequestBody List<RespostaFormularioDigitadoTO> listaTO) throws Exception {
		leituraFormulariosService.savarRespostaFormularioDigitado(listaTO);
		return new ResponseEntity<RespostaFormularioDigitadoTO>(HttpStatus.OK);
    }
	
	@RequestMapping(value = "/getRelatorioFormulario", method = RequestMethod.GET)
	public ResponseEntity<List<Object>> getRelatorioFormulario() throws Exception {
		return new ResponseEntity<List<Object>>(HttpStatus.OK);
	}
}
