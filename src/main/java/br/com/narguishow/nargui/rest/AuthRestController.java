package br.com.narguishow.nargui.rest;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.narguishow.nargui.auth.AuthService;

@RestController
@RequestMapping("/autenticar")
public class AuthRestController {

	@Autowired
	private AuthService authService;
	
	
	@RequestMapping(value="/logar", method=RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public ResponseEntity<UserDetails> autenticar(String username, String password) {
		return new ResponseEntity<UserDetails>(authService.login(username, password), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/autenticado", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> isAutenticado(HttpServletRequest httpServletRequest) {
		return new ResponseEntity<String>(httpServletRequest.getRemoteUser(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/logout", method = RequestMethod.POST)
	public ResponseEntity<?> logout(HttpServletRequest httpServletRequest) {
		if(httpServletRequest.getRemoteUser() != null) {
			authService.logout(httpServletRequest.getRemoteUser());
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<UserDetails> getUsuarioLogado() {
		UserDetails usuario = authService.getUsuarioAutenticado();
		if(usuario == null) {
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);	
		}
		return new ResponseEntity<UserDetails>(usuario, HttpStatus.OK);
	}
}
