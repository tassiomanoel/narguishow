package br.com.narguishow.nargui.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.narguishow.nargui.negocio.ent.Usuario;
import br.com.narguishow.nargui.services.UsuarioService;

//@RestController
//@RequestMapping("/usuario")
public class UsuarioRestController {

	@Autowired
	private UsuarioService userService;
	
	@RequestMapping(method= RequestMethod.GET)
	public ResponseEntity<List<Usuario>>listarTodosUsuarios(){
		return new ResponseEntity<List<Usuario>>(userService.listarTodosUsuarios(), HttpStatus.OK);
	}
	
	@RequestMapping(value="/{login}", method=RequestMethod.GET)
	public ResponseEntity<Usuario> getNameUser(@PathVariable String nome){
		return new ResponseEntity<Usuario>(userService.findByUsername(nome), HttpStatus.OK);
	}
}
