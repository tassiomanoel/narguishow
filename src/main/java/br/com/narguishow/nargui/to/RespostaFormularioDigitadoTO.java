package br.com.narguishow.nargui.to;

import java.util.Date;
import java.util.List;

public class RespostaFormularioDigitadoTO {

	public Integer idResposta;
	public Integer idRespostaFormulario;
	public String pathImagem;
	public String pathResposta;
	public String textoResposta;
	public Integer idPergunta;
	public String nomePergunta;
	public Date dataEnvio;
	public Integer tipoFormulario;
	
	public RespostaFormularioDigitadoTO() {}
	
	public RespostaFormularioDigitadoTO(Integer idResposta, Integer idRespostaFormulario,
			String pathResposta, String textoResposta, Integer idPergunta, String nomePergunta) {
		super();
		this.idResposta = idResposta;
		this.idRespostaFormulario = idRespostaFormulario;
		this.pathResposta = pathResposta;
		this.textoResposta = textoResposta;
		this.idPergunta = idPergunta;
		this.nomePergunta = nomePergunta;
	}
	
	public RespostaFormularioDigitadoTO(Integer idResposta, Integer idRespostaFormulario,
			String pathResposta, String textoResposta, Integer idPergunta, String nomePergunta, Date dataEnvio, Integer tipoFormulario) {
		super();
		this.idResposta = idResposta;
		this.idRespostaFormulario = idRespostaFormulario;
		this.pathResposta = pathResposta;
		this.textoResposta = textoResposta;
		this.idPergunta = idPergunta;
		this.nomePergunta = nomePergunta;
		this.dataEnvio = dataEnvio;
		this.tipoFormulario = tipoFormulario;
	}
	
	public RespostaFormularioDigitadoTO(Integer idRespostaFormulario, Integer idResposta, String textoResposta, String nomePergunta) {
		super();
		this.idRespostaFormulario = idRespostaFormulario;
		this.idResposta = idResposta;
		this.textoResposta = textoResposta;
		this.nomePergunta = nomePergunta;
	}
}
