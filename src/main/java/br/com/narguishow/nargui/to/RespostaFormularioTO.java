package br.com.narguishow.nargui.to;

import java.util.Date;

public class RespostaFormularioTO {

	public Integer id;
	public Date dataEnvio;
	public Date dataLeitura;
	public String msgErroLeitura;
	public String pathImagem;
}
