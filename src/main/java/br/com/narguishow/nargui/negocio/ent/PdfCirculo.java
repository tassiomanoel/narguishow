package br.com.narguishow.nargui.negocio.ent;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

@Entity
public class PdfCirculo {

	@Id @GeneratedValue(strategy = GenerationType.AUTO) public Integer id;

	@Column public Float x;

	@Column public Float y;

	@Column public Float w;
	
	@Column public Float h;
	
	@OneToMany(mappedBy="circulo") @OrderBy(value = "id") public List<PdfOpcao> opcoes;

}