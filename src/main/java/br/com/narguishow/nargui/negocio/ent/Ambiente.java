package br.com.narguishow.nargui.negocio.ent;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Ambiente {

	@Id @GeneratedValue(strategy = GenerationType.AUTO) public Integer id;

	@Column public String nome;

	@Column public String sigla;
}