package br.com.narguishow.nargui.negocio.web;

import java.io.Serializable;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Repository;

@Repository
@Scope(value = "request", proxyMode = ScopedProxyMode.DEFAULT)
public class DadosRequisicaoWeb implements Serializable {
	
	private static final long serialVersionUID = 1L;

}
 