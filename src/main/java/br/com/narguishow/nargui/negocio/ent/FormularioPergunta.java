package br.com.narguishow.nargui.negocio.ent;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class FormularioPergunta {

	@Id @GeneratedValue(strategy = GenerationType.AUTO) public Integer id;

	@Column public Float numPergunta;

	@Column public Float espaco;

	@Column public Integer numLinhasRespostaAberta;

	@Column public Integer numColunasOpcoes;

	@ManyToOne @JoinColumn(name="formulario_id") public Formulario formulario;

	@ManyToOne @JoinColumn(name="formulario_secao_id") public FormularioSecao formularioSecao;

	@ManyToOne @JoinColumn(name="pergunta_id") public Pergunta pergunta;

}