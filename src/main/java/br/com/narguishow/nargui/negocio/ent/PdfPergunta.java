package br.com.narguishow.nargui.negocio.ent;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

@Entity
public class PdfPergunta {

	@Id @GeneratedValue(strategy = GenerationType.AUTO) public Integer id;

	@Column public Float numPergunta;

	@ManyToOne @JoinColumn(name="pdf_id") public Pdf pdf;

	@ManyToOne @JoinColumn(name="pergunta_id") public Pergunta pergunta;

	@ManyToOne @JoinColumn(name="formulario_pergunta_id") public FormularioPergunta formularioPergunta;

	@ManyToOne @JoinColumn(name="retangulo_id") public PdfRetangulo retangulo;

	@OneToMany(mappedBy = "pdfPergunta") @OrderBy(value = "id") public List<PdfOpcao> opcoes;
}