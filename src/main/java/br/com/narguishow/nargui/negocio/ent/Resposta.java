package br.com.narguishow.nargui.negocio.ent;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

@Entity
public class Resposta {

	@Id @GeneratedValue(strategy = GenerationType.AUTO) public Integer id;

	@Column public Float numResposta;

	@Column public String pathResposta;

	@Column public String textoResposta;
	
	@ManyToOne @JoinColumn(name="pergunta_id") public Pergunta pergunta;

	@ManyToOne @JoinColumn(name="resposta_formulario_id") public RespostaFormulario respostaFormulario;

	@ManyToOne @JoinColumn(name="pdf_pergunta_id") public PdfPergunta pdfPergunta;

	@OneToMany(mappedBy = "resposta") @OrderBy(value = "id") public List<RespostaOpcao> respostasOpcoes;
}