package br.com.narguishow.nargui.negocio.ent;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

@Entity
public class TipoPerguntaFechada {

	@Id @GeneratedValue(strategy = GenerationType.AUTO) public Integer id;

	@Column public String textoExplicativo;

	@ManyToOne @JoinColumn(name="ambiente_id") public Ambiente ambiente;

	@OneToMany(mappedBy = "tipoPerguntaFechada") @OrderBy(value = "numOpcao") public List<Opcao> opcoes;
}