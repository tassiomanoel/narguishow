package br.com.narguishow.nargui.negocio.ent;

public enum Perfil {
	A("Gestor"), R("Respondente");
	
	public String nome;
	
	private Perfil(String nome) {
		this.nome = nome;
	}

	public boolean getIsAdministrador() {
		return this.equals(A);
	}

	public boolean getIsRespondente() {
		return this.equals(R);
	}
}