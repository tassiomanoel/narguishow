package br.com.narguishow.nargui.negocio.ent;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OrderBy;

@Entity
public class Grupo {

	@Id @GeneratedValue(strategy = GenerationType.AUTO) public Integer id;

	@Column public String nome;

	@ManyToOne @JoinColumn(name="ambiente_id") public Ambiente ambiente;

	@ManyToMany(fetch = FetchType.LAZY) @JoinTable(name = "grupo_pessoa", joinColumns = @JoinColumn(name = "idGrupo"), inverseJoinColumns = @JoinColumn(name = "idPessoa")) @OrderBy("nome") public Set<Pessoa> pessoas;
}