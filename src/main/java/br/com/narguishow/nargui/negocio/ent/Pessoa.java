package br.com.narguishow.nargui.negocio.ent;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Pessoa {

	@Id @GeneratedValue(strategy = GenerationType.AUTO) public Integer id;

	@Column public String nome;

	@ManyToOne @JoinColumn(name="ambiente_id") public Ambiente ambiente;
}