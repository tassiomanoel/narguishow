package br.com.narguishow.nargui.negocio.ent;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Pergunta {
	
	public final static Integer PERGUNTA_IDENTIFICADORA_NOME = 2;

	@Id @GeneratedValue(strategy = GenerationType.AUTO) public Integer id;

	@Column public String texto;

	@Enumerated(EnumType.STRING) public PerguntaTipo tipo;

	@ManyToOne @JoinColumn(name="tipo_pergunta_fechada_id") public TipoPerguntaFechada tipoPerguntaFechada;

	@ManyToOne @JoinColumn(name="ambiente_id") public Ambiente ambiente;
}