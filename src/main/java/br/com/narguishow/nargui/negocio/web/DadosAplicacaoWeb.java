package br.com.narguishow.nargui.negocio.web;

import java.io.Serializable;

import org.springframework.stereotype.Repository;

@Repository
public class DadosAplicacaoWeb implements Serializable {

	private static final long serialVersionUID = 1L;

	public String pathCompletoDiretorioAplicacao;
}
