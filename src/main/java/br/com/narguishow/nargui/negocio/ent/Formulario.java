package br.com.narguishow.nargui.negocio.ent;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

@Entity
public class Formulario {

	@Id @GeneratedValue(strategy = GenerationType.AUTO) public Integer id;

	@Column public String nome;

	@Column public String pathLogo;

	@ManyToOne @JoinColumn(name="ambiente_id")public Ambiente ambiente;

	@ManyToOne @JoinColumn(name="usuario_cadastro_id") public Usuario usuarioCadastro;

	@OneToMany(mappedBy = "formulario") @OrderBy(value = "numSecao") public List<FormularioSecao> formularioSecao;

	@OneToMany(mappedBy = "formulario") @OrderBy(value = "numPergunta") public List<FormularioPergunta> perguntas;

	@OneToMany(mappedBy = "formulario") @OrderBy(value = "id") public List<Pdf> pdfs;
}