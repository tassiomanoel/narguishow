package br.com.narguishow.nargui.negocio.ent;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class PdfOpcao {

	@Id @GeneratedValue(strategy = GenerationType.AUTO) public Integer id;

	@ManyToOne @JoinColumn(name="pdf_pergunta_id") public PdfPergunta pdfPergunta;

	@ManyToOne @JoinColumn(name="opcao_id") public Opcao opcao;

	@ManyToOne @JoinColumn(name="circulo_id") public PdfCirculo circulo;
}