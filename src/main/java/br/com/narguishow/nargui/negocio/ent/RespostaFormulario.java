package br.com.narguishow.nargui.negocio.ent;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

@Entity
public class RespostaFormulario {

	@Id @GeneratedValue(strategy = GenerationType.AUTO) public Integer id;

	@Column public String pathImagem;

	@Column public String pathImagemDebug;

	@Column public Date dataEnvio;

	@Column public Date dataLeitura;

	@Column public String msgErroLeitura;

	@ManyToOne @JoinColumn(name="ambiente_id") public Ambiente ambiente;

	@ManyToOne @JoinColumn(name="aplicacao_usuario_id")public AplicacaoUsuario aplicacaoUsuario;

	@ManyToOne @JoinColumn(name="formulario_id") public Formulario formulario;

	@ManyToOne @JoinColumn(name="pdf_id") public Pdf pdf;

	@OneToMany(mappedBy = "respostaFormulario") @OrderBy(value = "numResposta") public List<Resposta> respostas;
}