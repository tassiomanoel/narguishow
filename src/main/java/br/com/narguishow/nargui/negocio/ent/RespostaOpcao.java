package br.com.narguishow.nargui.negocio.ent;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class RespostaOpcao {

	@Id @GeneratedValue(strategy = GenerationType.AUTO) public Integer id;

	@ManyToOne @JoinColumn(name="resposta_id") public Resposta resposta;

	@ManyToOne @JoinColumn(name="opcao_id")public Opcao opcao;

	@ManyToOne @JoinColumn(name="pdf_opcao_id") public PdfOpcao pdfOpcao;
}