package br.com.narguishow.nargui.negocio.ent;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

@Entity
public class Pdf {

	@Id @GeneratedValue(strategy = GenerationType.AUTO) public Integer id;

	@Column public String pathArquivo;

	@ManyToOne @JoinColumn(name="formulario_id") public Formulario formulario;

	@ManyToOne @JoinColumn(name="guia_sup_esq_id") public PdfRetangulo guiaSupEsq;

	@ManyToOne @JoinColumn(name="guia_sup_dir_id") public PdfRetangulo guiaSupDir;

	@ManyToOne @JoinColumn(name="guia_inf_esq_id") public PdfRetangulo guiaInfEsq;

	@ManyToOne @JoinColumn(name="guia_inf_dir_id") public PdfRetangulo guiaInfDir;

	@ManyToOne @JoinColumn(name="qr_code_id") public PdfRetangulo qrCode;

	@ManyToOne @JoinColumn(name="bar_code128_id") public PdfRetangulo barCode128;

	@ManyToOne @JoinColumn(name="ambiente_id") public Ambiente ambiente;

	@ManyToOne @JoinColumn(name="usuario_geracao_id") public Usuario usuarioGeracao;

	@OneToMany(mappedBy = "pdf") @OrderBy(value = "numPergunta") public List<PdfPergunta> perguntas;

}