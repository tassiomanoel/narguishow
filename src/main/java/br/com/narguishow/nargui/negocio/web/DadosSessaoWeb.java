package br.com.narguishow.nargui.negocio.web;

import java.io.Serializable;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Repository;

import br.com.narguishow.nargui.negocio.ent.Perfil;
 
@Repository
@Scope(value = "session", proxyMode = ScopedProxyMode.DEFAULT)
public class DadosSessaoWeb implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public Integer idUsuarioLogado;

	public Perfil perfilUsuarioLogado;
}
