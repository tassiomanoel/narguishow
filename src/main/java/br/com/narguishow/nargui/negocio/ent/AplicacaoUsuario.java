package br.com.narguishow.nargui.negocio.ent;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class AplicacaoUsuario {

	@Id @GeneratedValue(strategy = GenerationType.AUTO) public Integer id;

	@ManyToOne(fetch = FetchType.LAZY) @JoinColumn(name="aplicacao_id")public Aplicacao aplicacao;

	@ManyToOne(fetch = FetchType.LAZY) @JoinColumn(name="pessoa_id") public Pessoa pessoa;
}