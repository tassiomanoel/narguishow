package br.com.narguishow.nargui.negocio.ent;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

@Entity
public class PdfRetangulo {

	@Id @GeneratedValue(strategy = GenerationType.AUTO) public Integer id;

	@Column public Float x;

	@Column public Float y;

	@Column public Float w;

	@Column public Float h;

	@OneToMany(mappedBy="retangulo") @OrderBy(value = "id") public List<PdfPergunta> perguntas;
	
	@OneToMany(mappedBy="barCode128") @OrderBy(value = "id") public List<Pdf> bars;
	@OneToMany(mappedBy="qrCode") @OrderBy(value = "id") public List<Pdf> qrs;
	
	@OneToMany(mappedBy="guiaSupEsq") @OrderBy(value = "id") public List<Pdf> se;
	@OneToMany(mappedBy="guiaSupDir") @OrderBy(value = "id") public List<Pdf> sd;
	@OneToMany(mappedBy="guiaInfDir") @OrderBy(value = "id") public List<Pdf> idirs;
	@OneToMany(mappedBy="guiaInfEsq") @OrderBy(value = "id") public List<Pdf> ie;
}