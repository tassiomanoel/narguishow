package br.com.narguishow.nargui.negocio.ent;

public enum TipoCodigoDeBarras {
	Q("QR Code"), B("Código de barras 128");
	
	public String nome;
	
	private TipoCodigoDeBarras(String nome) {
		this.nome = nome;
	}

	public boolean getIsQrCode() {
		return this.equals(Q);
	}

	public boolean getIsCodigoDeBarras128() {
		return this.equals(B);
	}
}