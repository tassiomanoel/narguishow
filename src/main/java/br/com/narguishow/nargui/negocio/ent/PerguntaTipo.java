package br.com.narguishow.nargui.negocio.ent;

public enum PerguntaTipo {
	T("Texto explicativo"), A("Aberta"), D("Data"), P("Telefone"), F("Fechada"), C("CPF"), N("NOME");
	
	public String nome;
	
	private PerguntaTipo(String nome) {
		this.nome = nome;
	}

	public boolean getIsTextoExplicativo() {
		return this.equals(T);
	}

	public boolean getIsAberta() {
		return this.equals(A);
	}

	public boolean getIsData() {
		return this.equals(D);
	}

	public boolean getIsTelefone() {
		return this.equals(P);
	}

	public boolean getIsFechada() {
		return this.equals(F);
	}

	public boolean getIsNome() {
		return this.equals(N);
	}

	public boolean getIsCPF() {
		return this.equals(C);
	}
}