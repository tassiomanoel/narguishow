package br.com.narguishow.nargui.negocio.ent;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Role {

	@Id
	public Integer id;

	@Column
	public String nome;
}
