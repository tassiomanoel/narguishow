package br.com.narguishow.nargui.negocio.ent;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "usuario")
public class UsuarioAutorizacao {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer id;

	@Enumerated(EnumType.STRING)
	public Perfil perfil;

	@ManyToOne
	@JoinColumn(name = "ambiente_id")
	public Ambiente ambiente;

	@ManyToOne
	@JoinColumn(name = "usuario_id")
	public Usuario usuario;
}