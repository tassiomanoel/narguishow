package br.com.narguishow.nargui.negocio.ent;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Opcao {

	@Id @GeneratedValue(strategy = GenerationType.AUTO) public Integer id;

	@Column public Integer numOpcao;

	@Column public String texto;

	@ManyToOne @JoinColumn(name="tipo_pergunta_fechada_id") public TipoPerguntaFechada tipoPerguntaFechada;

}