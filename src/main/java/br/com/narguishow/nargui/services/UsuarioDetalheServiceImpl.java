package br.com.narguishow.nargui.services;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.narguishow.nargui.negocio.ent.Usuario;
import br.com.narguishow.nargui.negocio.ent.UsuarioRole;
import br.com.narguishow.nargui.repository.UsuarioRepository;

@Service
public class UsuarioDetalheServiceImpl implements UserDetailsService {

	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
		Usuario usuario = usuarioRepository.findByUsername(login);
		
		Set<GrantedAuthority> grantedAuthorities = new HashSet<GrantedAuthority>();
		for(UsuarioRole role : usuario.usuarioRole){
			grantedAuthorities.add(new SimpleGrantedAuthority(role.role.nome));
		}
		
		return new User(usuario.username, usuario.senhaAberta, grantedAuthorities);
	}

}
