package br.com.narguishow.nargui.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.narguishow.nargui.negocio.ent.Usuario;
import br.com.narguishow.nargui.negocio.ent.UsuarioRole;
import br.com.narguishow.nargui.repository.UsuarioRepository;
import br.com.narguishow.nargui.repository.UsuarioRoleRepository;

@Service
public class UsuarioService {

	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Autowired
    private UsuarioRoleRepository usuarioRoleRepository;
    
	
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public Usuario findByUsername(String login){
		return usuarioRepository.findByUsername(login);
	}
	
	@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
	public List<Usuario> listarTodosUsuarios(){
		return usuarioRepository.findAll();
	}

	
    public void salvar(Usuario usuario) {
        usuario.usuarioRole = new ArrayList<UsuarioRole>(usuarioRoleRepository.findAll());
        usuarioRepository.save(usuario);
    }
}
