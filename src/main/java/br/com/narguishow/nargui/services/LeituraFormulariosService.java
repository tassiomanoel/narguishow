package br.com.narguishow.nargui.services;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.narguishow.nargui.negocio.ent.Pdf;
import br.com.narguishow.nargui.negocio.ent.Resposta;
import br.com.narguishow.nargui.negocio.ent.RespostaFormulario;
import br.com.narguishow.nargui.repository.RespostaFormularioRepository;
import br.com.narguishow.nargui.repository.RespostaRepository;
import br.com.narguishow.nargui.to.RespostaFormularioDigitadoTO;
import br.com.narguishow.nargui.to.RespostaFormularioTO;

@Service
public class LeituraFormulariosService extends QuestService {

	@Autowired
	private RespostaFormularioRepository respostaFormularioRepository;

	@Autowired
	private RespostaRepository respostaRepository;

	@Transactional
	public Integer salvar(Integer idPdf, BufferedImage bi) throws Exception {
		Pdf pdf = entityManager.find(Pdf.class, idPdf);

		RespostaFormulario resposta = new RespostaFormulario();

		resposta.pdf = pdf;
		resposta.formulario = pdf.formulario;
		resposta.dataEnvio = new Date();
		resposta.ambiente = getAmbienteCorrente();
		resposta.respostas = new ArrayList<>();

		respostaFormularioRepository.save(resposta);

		return resposta.id;
	}

	public List<RespostaFormularioTO> listaRespostaFormularioPorPeriodo(Date dataInicio, Date dataFim) {
		List<RespostaFormulario> listaPorPeriodo = respostaFormularioRepository
				.listaRespostaFormularioPorPeriodo(dataInicio, dataFim);
		List<RespostaFormularioTO> listaPorPeriodoDTO = new ArrayList<>();
		RespostaFormularioTO dto = null;

		for (RespostaFormulario resposta : listaPorPeriodo) {
			dto = new RespostaFormularioTO();
			dto.id = resposta.id;
			dto.dataEnvio = resposta.dataEnvio;
			dto.dataLeitura = resposta.dataLeitura;
			dto.msgErroLeitura = resposta.msgErroLeitura;
			listaPorPeriodoDTO.add(dto);
		}

		return listaPorPeriodoDTO;
	}

	public List<Integer> getContadorRespostaFormulario() {
		List<Integer> retorno = new ArrayList<>();
		retorno.add(respostaFormularioRepository.getContadorFormulariosAguardandoDigitacao());
		List<Integer> listaFormulario = respostaRepository.getListaTodosFormularioProcessoIniciado();
		List<Resposta> listaResposta = respostaRepository.getListaRespostaComRespostas(listaFormulario);
		for (Resposta resposta : listaResposta) {
			if (resposta.textoResposta == null || resposta.textoResposta.equalsIgnoreCase("")) {
				listaFormulario.remove(resposta.respostaFormulario.id);
			}
		}
		retorno.add(listaFormulario.size());
		return retorno;
	}

	public List<RespostaFormularioDigitadoTO> getRespostaFormularioTextoNulo() {
		List<RespostaFormularioDigitadoTO> lista = respostaFormularioRepository.getRespostaFormularioTextoNulo();
		List<RespostaFormularioDigitadoTO> listaRetorno = new ArrayList<>();
		for (RespostaFormularioDigitadoTO to : lista) {
			listaRetorno.add(to);
		}
		return listaRetorno;
	}

	public void savarRespostaFormularioDigitado(List<RespostaFormularioDigitadoTO> listaTO) {
		for (RespostaFormularioDigitadoTO to : listaTO) {
			Resposta resposta = respostaRepository.findOne(to.idResposta);
			resposta.textoResposta = to.textoResposta;
			respostaRepository.save(resposta);
		}
	}

}