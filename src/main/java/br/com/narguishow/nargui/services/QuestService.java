package br.com.narguishow.nargui.services;

import java.util.Collection;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.narguishow.nargui.negocio.ent.Ambiente;
import br.com.narguishow.nargui.negocio.ent.Usuario;

public abstract class QuestService {

	@PersistenceContext
	protected EntityManager entityManager;
	
	public Logger logger = Logger.getLogger(this.getClass().toString());
	
	public Ambiente getAmbienteCorrente() {
		// implementar corretamente após ter autenticação
		return entityManager.find(Ambiente.class, 1);
	}

	public Usuario getUsuarioLogado() {
		// implementar corretamente após ter autenticação
		return entityManager.find(Usuario.class, 1);
	}

	public boolean isEmpty(Object o) {
		return o == null || o.toString().trim().length() == 0;
	}

	public boolean isEmpty(Object... objs) {
		if (objs == null) {
			return true;
		}

		for (Object o : objs) {
			if (o == null || o.toString().trim().length() == 0) {
				return true;
			}
		}
		return false;
	}

	public boolean isEmpty(Collection c) {
		return c == null || c.isEmpty();
	}

	public boolean hasValue(Object o) {
		return o != null && o.toString().trim().length() > 0;
	}

	public boolean hasValue(Object[] o) {
		return o != null && o.length > 0;
	}

	public boolean hasValue(int[] o) {
		return o != null && o.length > 0;
	}

	public boolean hasValue(Collection c) {
		return c != null && !c.isEmpty();
	}
}
