package br.com.narguishow.nargui;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableAspectJAutoProxy
@EnableJpaRepositories(basePackages="br.com.narguishow.nargui")
@EntityScan("br.com.narguishow.nargui.*")
//scanear pacotes de dentro da aplicacao e de dependencias
@ComponentScan(value={"br.com.narguishow.nargui, br.com.narguishow.core.home"})
@EnableScheduling
@EnableAutoConfiguration
@SpringBootApplication
public class AplicacaoNarguishow extends SpringBootServletInitializer {
	
//	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
//        return application.sources(AplicacaoNarguishow.class);
//    }
//	
//	public static ConfigurableApplicationContext main(String[] args) {
//		ConfigurableApplicationContext ac = SpringApplication.run(AplicacaoNarguishow.class, args);
//		System.out.println("Aplicação iniciada com sucesso!");
//		return ac;
//	}
	
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(AplicacaoNarguishow.class);
    }
	
	public static void main(String[] args) {
		SpringApplication.run(AplicacaoNarguishow.class, args);
		System.out.println("Aplicação iniciada com sucesso!");
	}
}