package br.com.narguishow.nargui.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.narguishow.nargui.negocio.ent.UsuarioRole;

public interface UsuarioRoleRepository extends JpaRepository<UsuarioRole, Integer>{

}
