package br.com.narguishow.nargui.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.narguishow.nargui.negocio.ent.RespostaFormulario;
import br.com.narguishow.nargui.to.RespostaFormularioDigitadoTO;

public interface RespostaFormularioRepository extends JpaRepository<RespostaFormulario, Integer>{
	
	@Query("from RespostaFormulario where dataEnvio BETWEEN :dataInicio AND :dataFim")
	List<RespostaFormulario> listaRespostaFormularioPorPeriodo(@Param("dataInicio") Date dataInicio, @Param("dataFim") Date dataFim );
	
	@Query("from RespostaFormulario where dataLeitura is null and msgErroLeitura is null order by dataEnvio desc")
	List<RespostaFormulario> listaRespostaFormularioPendentes() throws Exception;
	
	@Query("select count(*) from RespostaFormulario rf join rf.respostas r where r.textoResposta is null")
	Integer getContadorFormulariosAguardandoDigitacao();
	
	@Query("select count(*) from RespostaFormulario rf join rf.respostas r where r.textoResposta is not null")
	Integer getContadorFormulariosProcessados();
	
	@Query("select new br.com.narguishow.nargui.to.RespostaFormularioDigitadoTO(res.id, res.respostaFormulario.id, res.pathResposta, "
			+ " res.textoResposta, res.pergunta.id, per.texto) "
			+ " from Resposta res join res.pergunta per where res.textoResposta is null and res.pathResposta is not null "
			+ " and res.respostaFormulario.id = (select max(rf.id) from RespostaFormulario rf join rf.respostas r where "
			+ " r.textoResposta is null and r.pathResposta is not null order by rf.dataEnvio desc)")
	List<RespostaFormularioDigitadoTO> getRespostaFormularioTextoNulo();
	
	@Query("select new br.com.narguishow.nargui.to.RespostaFormularioDigitadoTO(form.id, res.id, res.textoResposta, per.texto) "
			+ " from RespostaFormulario form join form.respostas res join res.pergunta per where form.id in (:um, :dois)")
	List<RespostaFormularioDigitadoTO> getRelatorioFormulario(@Param("um") Integer um,
			@Param("dois") Integer dois);
}
