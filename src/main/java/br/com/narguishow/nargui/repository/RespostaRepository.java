package br.com.narguishow.nargui.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.narguishow.nargui.negocio.ent.Resposta;

public interface RespostaRepository extends JpaRepository<Resposta, Integer>{
	
	@Query(value="select distinct resposta_formulario_id from resposta where texto_resposta is not null and path_resposta is not null", nativeQuery=true)
	List<Integer> getListaTodosFormularioProcessoIniciado();
	
	@Query(value="select * from resposta where resposta_formulario_id IN(:listaFormulario)  and path_resposta is not null", nativeQuery=true)
	List<Resposta> getListaRespostaComRespostas(@Param("listaFormulario") List<Integer> listaFormulario);
}
