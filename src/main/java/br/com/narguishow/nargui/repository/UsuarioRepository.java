package br.com.narguishow.nargui.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.narguishow.nargui.negocio.ent.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Integer>{
	
	@Query("select u from Usuario u where u.username = :login and u.senhaAberta = :password")
	Usuario buscarUsuarioBaseDados(@Param("login") String login, @Param("password") String password);
	
	Usuario findByUsername(String username);
}
