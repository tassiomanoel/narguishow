package br.com.narguishow.nargui.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.narguishow.nargui.negocio.ent.Formulario;

public interface FormularioRepository extends JpaRepository<Formulario, Integer>{
	
}
