package br.com.narguishow.nargui.auth;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
public interface AuthService {

	UserDetails getUsuarioAutenticado();
	
	UserDetails login(String login, String password);
	
	void logout(String usuario);
}
