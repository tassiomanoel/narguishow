package br.com.narguishow.nargui.auth;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import br.com.narguishow.nargui.negocio.ent.Usuario;
import br.com.narguishow.nargui.repository.UsuarioRepository;
import br.com.narguishow.nargui.services.UsuarioDetalheServiceImpl;

@Service
public class AuthServiceImpl implements AuthService {

	@Autowired
	private UsuarioDetalheServiceImpl usuarioDetalheService;
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	private static final Logger logger = LoggerFactory.getLogger(AuthServiceImpl.class);

	@Override
	public UserDetails getUsuarioAutenticado() {
		Object user = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if(user instanceof UserDetails){
			return ((UserDetails) user);
		}
		return null;
	}

	@Override
	public UserDetails login(String login, String password) {
		UserDetails principal =  usuarioDetalheService.loadUserByUsername(login);
		
		UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(principal, password, principal.getAuthorities());
		auth.setDetails(principal);
		authenticationManager.authenticate(auth);
		
		if(auth.isAuthenticated()){
			SecurityContextHolder.getContext().setAuthentication(auth);
			logger.debug("Logado na aplicação");
		}
		return principal;
	}

	@Override
	public void logout(String usuario) {
		UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(null, null);
		authenticationManager.authenticate(auth);
		SecurityContextHolder.getContext().setAuthentication(auth);
		logger.info("Usuário fez logof.");
	}
}
