'use strict';

angular.module('narguishowApp')
    .factory('AuthRota', function AuthRota($resource) {
        return $resource('autenticar', {}, {
            'get': { method: 'GET', params: {}, isArray: false,
                interceptor: {
                    response: function(response) {
                        return response;
                    }
                }
            }
        });
    });
