'use strict';

angular.module('narguishowApp')
    .factory('Principal', function Principal($q, $rootScope, $state, AuthRota) {
        var _identidade,
            _autenticado = false;

        return {
            isIdentidadeResolvida: function () {
                return angular.isDefined(_identidade);
            },
            
            isAutenticado: function () {
                return _autenticado;
            },
            
            autenticado: function (identidade) {
                _identidade = identidade;
                _autenticado = identidade !== null;
            },
            
            identidade: function (force) {
                var deferred = $q.defer();

                if (force === true) {
                    _identidade = undefined;
                }

                if (angular.isDefined(_identidade)) {
                    deferred.resolve(_identidade);
                    return deferred.promise;
                }

                AuthRota.get().$promise
                    .then(function (response) {
                        _identidade = response.data;
                        _autenticado = true;
                        deferred.resolve(_identidade);
                        $rootScope.usuarioLogado = response.data.username;
                        
                        if($rootScope.toState.url == "/login"){
							$state.go("home");
        				}
                    })
                    .catch(function() {
                        _identidade = null;
                        _autenticado = false;
                        deferred.resolve(_identidade);
                    });
                return deferred.promise;
            }
        };
    });
