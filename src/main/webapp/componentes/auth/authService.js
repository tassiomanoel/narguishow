'use strict';

angular.module('narguishowApp')
	.factory('AuthService', function ($rootScope, $http, $q, $state, Principal) {
		var authService = {};
		var deferred = $q.defer();
		  authService.login = function (credentials, callback) {
			var cb = callback || angular.noop;
			var deferred = $q.defer();

			var data = 'username=' + encodeURIComponent(credentials.username) +
		     		'&password=' + encodeURIComponent(credentials.password);  
		    return $http
				      .post('/autenticar/logar', data, {
				    	  headers: {
		                      'Content-Type': 'application/x-www-form-urlencoded'
		                  }
				      }).then(function (response) {
						Principal.identidade(true).then(function(data) {
				    		  deferred.resolve(response.data);
	                      });
			    	  	return cb;
				      });
		  };
		 
		  authService.autorizar = function(force) {
              return Principal.identidade(force)
                  .then(function() {
                      var isAutenticado = Principal.isAutenticado();
                    //   if (isAutenticado) {
                    //       $state.go('home');
                    //   }
                    //   else {
                    //       $state.go('login');
					//   }
					
					//$state.go('home');
                  });
          };
          
          authService.logout = function() {
        	  return $http.post('/autenticar/logout').then(function (response) {
	  					return response;
	  				}); 
          }
		 
		  return authService;
	});