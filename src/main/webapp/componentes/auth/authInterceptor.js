'use strict';

angular.module('narguishowApp')
	.config(function ($httpProvider) {
		$httpProvider.interceptors.push([
		  '$injector',
		  function ($injector) {
		    return $injector.get('AuthInterceptor');
		  }
		]);
	})
	
	.factory('AuthInterceptor', function ($rootScope, $q, $injector) {
		return {
			responseError: function (response) { 
				if(response.status == 404 || response.status == 401){
					var Auth = $injector.get('AuthService');
					var $state = $injector.get('$state');
					Auth.logout();
					$state.go('login');
				}				
				return $q.reject(response);
			}
		};
	})