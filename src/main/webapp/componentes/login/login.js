'use strict';

angular.module('narguishowApp')

	.config(function ($stateProvider) {
		$stateProvider
			.state('login', {
				url: '/login',
				views: {
					'login@': {
						templateUrl: 'componentes/login/login.html',
						controller: 'loginController'
					}
				},  
				data: {
					roles: []
				}
			});
	});

	// $(window).load(function() {
	// 	if(location.hash == "#!/login"){
	// 		$('.main-panel').addClass('painel-principal');
	// 	}
	// });

