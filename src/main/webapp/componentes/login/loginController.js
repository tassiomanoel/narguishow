'use strict';

angular.module('narguishowApp')
    .controller('loginController', function ($scope, $rootScope, $state, AuthService, Principal) {
		$scope.mensagemErroAuth = false;

		$scope.credencial = {
        		username: '',
        		password: ''
        };
        
        $scope.logar = function(credencial) {
        	AuthService.login(credencial).then(function(user){
				$('main-panel').removeAttr('style');
				Principal.autenticado(user.data);
				$state.go('home');
        	}, function(error) {
				$scope.mensagemErroAuth = true;
        	});
    	};

    	$scope.logout = function() {
    		AuthService.logout().then(function(response) {
				Principal.autenticado(null);
				$state.go('login');
    		});
		};

		$scope.alternarMenu = function(){
            $('body').toggleClass('sidebar-mini');                
        }
    });
