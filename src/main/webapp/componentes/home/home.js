'use strict';

angular.module('narguishowApp')

    .config(function ($stateProvider) {
        $stateProvider
            .state('home', {
                parent : 'site',
                url: '/',
                views : {
                    'content@' : {
                        templateUrl: 'componentes/home/home.html',
                        controller: 'homeController',
                    }
                },     
                data: {
                    roles: []
                }
            });
    });
    $(window).load(function() {
		if(location.hash == "#!/"){
		    $('.main-panel').removeClass('painel-principal');
		}
		
	});


