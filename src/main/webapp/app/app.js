'use strict';
angular.module('narguishowApp', ['ngResource','ngStorage','ui.bootstrap','ui.router', 'LocalStorageModule'])

	.run(function ($rootScope, $location, $window, $http, $state, $sessionStorage, $timeout, AuthService, Principal) {
		$rootScope.$on('$stateChangeStart', function (event, toState, toStateParams, fromState) {
			$rootScope.toState = toState;
			$rootScope.toStateParams = toStateParams;
			$rootScope.fromState = fromState;
			Principal.identidade(true);
		});

		$rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
			$window.document.title = toState.name;
		});
	})
	
	.config(function($stateProvider, $urlRouterProvider){
		$urlRouterProvider.otherwise('/');
		$stateProvider
			.state('site', {
				abstract: true,
				views : {
					'nav@' : {
						templateUrl : 'componentes/menu/menu.html',
						controller : 'menuController'
					},
					'header@' : {
						templateUrl: 'componentes/login/header.html',
						controller: 'loginController'	
					}
				}
				// resolve: {
				// 	authorize: ['AuthService',
				// 		function (AuthService) {
				// 			return AuthService.autorizar();
				// 		}
				// 	]
				// }
		});
		//$urlRouterProvider.otherwise('/login');
	})
;
