'use strict';

angular.module('narguishowApp')

    .config(function ($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('respostaFormulario', {
                parent : 'site',
                url: '/respostaFormulario',
                views : {
                    'content@' : {
                        templateUrl: 'app/entities/respostaFormulario/respostaFormulario.html',
                        controller: 'respostaFormularioController',
                    }
                },     
                data: {
                    roles: []
                }
            })
            .state('respostaFormulario.cadastrarResposta', {
                url: '/cadastrar-resposta',
                views : {
                    'content@' : {
                        templateUrl: 'app/entities/respostaFormulario/cadastrarResposta.html',
                        controller: 'respostaFormularioController',
                    }
                },     
                data: {
                    roles: []
                }
            })
	        .state('respostaFormulario.relatorio', {
	            url: '/relatorio',
	            views : {
	                'content@' : {
	                    templateUrl: 'app/entities/respostaFormulario/relatorio.html',
	                    controller: 'respostaFormularioController',
	                }
	            },     
	            data: {
	                roles: []
	            }
	        });
    });



