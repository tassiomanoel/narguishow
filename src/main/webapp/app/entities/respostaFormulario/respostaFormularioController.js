'use strict';

angular.module('narguishowApp')
    .controller('respostaFormularioController', function ($scope, $rootScope, $state, $http, RespostaFormularioService) {
		$scope.respostaFormularioDigitado = {};
		$scope.listaRelatorioFormulario = [];

		RespostaFormularioService.getContadorRespostaFormulario(function(response) {
    		$scope.agurdandoDigitacao = response[0];
    		$scope.processados = response[1];
    	});
    	
    	if($state.current.name == 'respostaFormulario.cadastrarResposta'){
    		RespostaFormularioService.getRespostaFormularioTextoNulo(function(response){
    			$scope.listaFormulario = response;
    			$scope.numeroFormulario = response[0].idRespostaFormulario;
    		});
		}
    	
    	if($state.current.name == 'respostaFormulario.relatorio'){
    		RespostaFormularioService.getRelatorioFormulario(function(response){
    			$scope.listaRelatorioFormularioOriginal = response;
    			$scope.listaRelatorioFormulario = response;
    			$scope.titulo = response[0];
    			$scope.carregarDatePicker();
    		});
		}
		
		$scope.salvarContinuar = function(){
			RespostaFormularioService.save($scope.listaFormulario, function(result){
				$state.go('respostaFormulario.cadastrarResposta', null, { reload: true });	
			});
		}

		$scope.salvarVoltar = function(){
			RespostaFormularioService.save($scope.listaFormulario, function(res) {
				$state.go('respostaFormulario');		
			});
		}
		
		$scope.buscarPorDataEnvio = function(){
			var dataEnviada = $("#idBuscarPorData").val();
			$scope.listaAlterada = [];
			$scope.tipoFormulario;
			
			if(dataEnviada == "" && $scope.tipoFormulario == undefined){
				$scope.listaRelatorioFormulario = $scope.listaRelatorioFormularioOriginal
			} else {
				angular.forEach($scope.listaRelatorioFormularioOriginal, function(formulario){
					var dataFormulario = new Date(formulario[0].dataEnvio);
					var dataFormualarioFormatada = moment(dataFormulario).format('DD/MM/YYYY');
					
					if($scope.tipoFormulario != undefined && $scope.tipoFormulario == formulario[0].tipoFormulario){
						if(dataEnviada == ""){
							$scope.listaAlterada.push(formulario);
						} else if(dataEnviada == dataFormualarioFormatada){
							$scope.listaAlterada.push(formulario);
						}
					} else if($scope.tipoFormulario == undefined){
						if(dataEnviada == dataFormualarioFormatada){
							$scope.listaAlterada.push(formulario);
						}
					} else if($scope.tipoFormulario == formulario[0].tipoFormulario && dataEnviada == dataFormualarioFormatada){
						$scope.listaAlterada.push(formulario);
					}
				});
				$scope.listaRelatorioFormulario = $scope.listaAlterada;
			}
    	};
		
		$scope.carregarDatePicker = function(){
			$('.datepicker').datetimepicker({
		        format: 'DD/MM/YYYY',
		        icons: {
		            time: "fa fa-clock-o",
		            date: "fa fa-calendar",
		            up: "fa fa-chevron-up",
		            down: "fa fa-chevron-down",
		            previous: 'fa fa-chevron-left',
		            next: 'fa fa-chevron-right',
		            today: 'fa fa-screenshot',
		            clear: 'fa fa-trash',
		            close: 'fa fa-remove'
		        }
		    });
		}
		
    });
