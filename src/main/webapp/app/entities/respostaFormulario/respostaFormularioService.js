'use strict';

angular.module('narguishowApp')
    .factory('RespostaFormularioService', function ($resource) {
        return $resource('respostaformulario/:param/:id', {
        	param : '@param'
        }, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' },
            'getContadorRespostaFormulario' : {
            	method: 'GET',
            	params: {
            		param: 'getContadorRespostaFormulario'
            	}, 
            	isArray: true
            },
            'getRespostaFormularioTextoNulo' : {
            	method: 'GET',
            	params: {
            		param: 'getRespostaFormularioTextoNulo'
            	}, 
            	isArray: true
            },
            'getRelatorioFormulario' : {
            	method: 'GET',
            	params: {
            		param: 'getRelatorioFormulario'
            	}, 
            	isArray: true
            }
        });
    });