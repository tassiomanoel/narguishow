'use strict';

angular.module('narguishowApp')

    .config(function ($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('usuario', {
                parent : 'site',
                url: '/usuario',
                views : {
                    'content@' : {
                        templateUrl: 'app/entities/usuario/usuario.html',
                        controller: 'usuarioController',
                    }
                },     
                data: {
                    roles: []
                }
            });
    });



