'use strict';

angular.module('narguishowApp')
    .factory('UsuarioService', function ($resource, DateUtils) {
        return $resource('narguishow/usuario/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });
