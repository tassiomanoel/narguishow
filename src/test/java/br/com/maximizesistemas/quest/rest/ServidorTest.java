package br.com.maximizesistemas.quest.rest;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import br.com.maximizesistemas.quest.AbstractTest;
import br.com.narguishow.nargui.AplicacaoNarguishow;

@Transactional 
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes=AplicacaoNarguishow.class)
@WebAppConfiguration	
public class ServidorTest extends AbstractTest {

	@Test public void testarSubidaServidor() throws Exception {
		//ConfigurableApplicationContext ac = AplicacaoNarguishow.main(new String[]{});

		try {
			Assert.assertEquals(404, get("quest/url/desconhecida"));
		} catch (Exception e) {
		}

		try {
			Assert.assertEquals(200, get("quest/#!/login"));
		} catch (Exception e) {
		}

		try {
			Assert.assertEquals(200, get("quest/"));
		} catch (Exception e) {
		}
		
		//ac.close();
	}
}
