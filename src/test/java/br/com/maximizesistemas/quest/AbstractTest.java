package br.com.maximizesistemas.quest;

import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.After;
import org.junit.Before;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import br.com.narguishow.nargui.negocio.ent.Perfil;
import br.com.narguishow.nargui.negocio.web.DadosSessaoWeb;
 
public abstract class AbstractTest {
	
	public static List<String> REMOCAO_USUARIOS_CODIGO_SISTEMA_ORIGEM = new ArrayList<String>();
	public static List<Integer> REMOCAO_USUARIOS_COD_USUARIO = new ArrayList<Integer>();
	public static List<String> REMOCAO_TURMAS_CODIGOS_SISTEMA_ORIGEM = new ArrayList<String>();
	public static List<Integer> REMOCAO_TURMAS_COD_TURMA = new ArrayList<Integer>();
	public static List<Integer> REMOCAO_AGENDAMENTOS = new ArrayList<Integer>();
	
//	@Autowired public ApplicationContext applicationContext;
//	
//	@Autowired public SessionFactory sessionFactory;
//
//	@Autowired 
//	public ControleDeTransacao controleDeTransacao;
	
	@PersistenceContext
	public EntityManager entityManager;

	public Logger logger = Logger.getLogger(this.getClass().toString());

	@Before public void before() throws Exception {
		if (entityManager == null) {
			Properties p = new Properties();
			p.load(this.getClass().getClassLoader().getResourceAsStream("fp.properties"));
//			this.urlDeAcessoDoAmbienteDeTestes = p.getProperty("fp-testes-url-do-ambiente-de-testes");
//			this.isDeveBuscarIpExternoParaHabilitarServidorIntegrado = p.getProperty("fp-testes-buscar-ip-externo-para-habilitar-servidor-externo").equals("true");
			return;
		}
		
//		((GenericApplicationContext) applicationContext).getBeanFactory().registerScope("request", new RequestScope());
//		((GenericApplicationContext) applicationContext).getBeanFactory().registerScope("session", new SessionScope());
		
		if (RequestContextHolder.getRequestAttributes() == null) {
			MockHttpServletRequest httpRequest = new MockHttpServletRequest();
			httpRequest.setSession(new MockHttpSession());
			RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(httpRequest));
		}
		
//		controleDeTransacao.begin();
	}

	@After public void after() {
		if (entityManager == null) return;
		
		//controleDeTransacao.rollback();
	}
	
	public void logar(Integer codUsuario) {
		DadosSessaoWeb dadosSessaoWeb = entityManager.find(DadosSessaoWeb.class, 1);
		dadosSessaoWeb.idUsuarioLogado = codUsuario;
		dadosSessaoWeb.perfilUsuarioLogado = Perfil.A;
	}

	public File getDirDesktopUsuario() {
		File user = new File(System.getProperty("user.home"));
		if (user.exists()) {
			File desktop = new File(user, "Desktop");
			if (!desktop.exists()) {
				desktop = new File(user, "Área de Trabalho");
			}
			return desktop;
		}
		return null;
	}
	
	public File getDirTemporario() {
		return new File(System.getProperty("java.io.tmpdir"));
	}
	
	public File getFileClasspath(String s) {
		return new File(this.getClass().getResource(s).getFile());
	}

	public boolean comparar(Float f1, Float f2) {
		if (f1 == null && f2 == null) return true;
		if (f1 == null) return false;
		if (f2 == null) return false;
		return Math.round(f1*100000f) == Math.round(f2*100000f); 
	}

	public int post(String path, String... params) throws MalformedURLException, IOException {
		String urlParameters = ""; 
		
		for (int i = 0; i < params.length; i = i + 2) {
			urlParameters = urlParameters + params[i] + "=" + params[i + 1] + "&";
		}
		
		byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);
		int postDataLength = postData.length;
		URL url = new URL("http://localhost:8080/" + path);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setDoOutput(true);
		conn.setInstanceFollowRedirects(false);
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		conn.setRequestProperty("charset", "utf-8");
		
		if (!urlParameters.isEmpty()) {
			conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
			conn.setUseCaches(false);
			try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
				wr.write(postData);
			}
		}

		int responseCode = conn.getResponseCode();
		return responseCode;
	}

	public int get(String path, String... params) throws MalformedURLException, IOException {
		String urlParameters = ""; 
		
		for (int i = 0; i < params.length; i = i + 2) {
			urlParameters = urlParameters + params[i] + "=" + params[i + 1];
		}
		
		URL url = new URL("http://localhost:8080/" + path + "?" + urlParameters);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setDoOutput(true);
		conn.setInstanceFollowRedirects(false);
		conn.setRequestMethod("GET");
		conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		conn.setRequestProperty("charset", "utf-8");

		int responseCode = conn.getResponseCode();
		return responseCode;
	}
}
