package br.com.maximizesistemas.quest.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import br.com.maximizesistemas.quest.AbstractTest;
import br.com.narguishow.nargui.AplicacaoNarguishow;
import br.com.narguishow.nargui.negocio.ent.Formulario;
import br.com.narguishow.nargui.negocio.ent.PdfCirculo;

@Transactional 
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes=AplicacaoNarguishow.class)
@WebAppConfiguration	
public class PersistenciaTest extends AbstractTest {

	@Test public void gravacaoERecuperacaoSemCommit() throws Exception {
		PdfCirculo c = new PdfCirculo();

		c.h = 1f;
		c.w = 1f;
		c.x = 1f;
		c.y = 1f;

		entityManager.persist(c);
		entityManager.flush();
		
		System.out.println(c.id);

		PdfCirculo c2 = entityManager.find(PdfCirculo.class, c.id);
		
		Assert.assertEquals(c.id, c2.id);
		Assert.assertEquals(c.x, c2.x);
		Assert.assertEquals(c.y, c2.y);
		Assert.assertEquals(c.w, c2.w);
		Assert.assertEquals(c.h, c2.h);
	}

	@Test public void formulario1() throws Exception {
		Formulario f = entityManager.find(Formulario.class, 1);
		Assert.assertEquals(1, f.ambiente.id.intValue());
		Assert.assertNotNull(f.pathLogo);
		Assert.assertEquals(28, f.perguntas.size());
	}
}
